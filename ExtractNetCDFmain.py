# -*- coding: utf-8 -*-
"""
@author: au302246
"""

import ExtractUtilities as EU
import plotting_from_json as plot

"""
The configuration file should be configured with:
    paths
    variables to extract
    depths to extract
    period of interest 
    how output should be stored
    
You may run the "API_get_basics" and inspect the "meta" returned to get info on
simuation period and depths

The variable names are exported to a csv file and can be copied into the config
file to get correct naming.

The script functions rely on the netCDF4 module an the plotly module. Make sure
they are installed before executing the main script "ExtractNetCDFmain.py" 

In the config file the section "output" has the following meaning:

"API_get"            : if set to TRUE a csv with avaliable variables are exported
"profiles"           : if set to TRUE output is saved as txt files
"collected_json"     : if set to TRUE a json file with output is saved

To plot specify the variable name ad depth in the config file.

"""
config_file = "config_json.json"

# first function to get basics about the simulation (variables and duration)
reguest_get = EU.API_get_basics(config_file)

# extract the data from the netCDF exported to txt files and/or json 
reguest_get_data = EU.API_get_data(config_file)

# plotting data via plotly for a specific variable and depth
plot.plotting(config_file)


# -*- coding: utf-8 -*-
"""
@author: au302246
"""

import netCDF4
import numpy
from datetime import datetime
import json
import os.path
import csv  


def API_get_basics(json_config_file, skip_output = None):
    
    with open(json_config_file, 'r') as f:
        config_dict = json.load(f)
    
    config_file_get = config_dict
    
    # getting overview of the variables available:    
    dict_APIget ={}
    dict_APIget["variables"] ={}
    with netCDF4.Dataset(config_file_get["paths"]["nc_file"]) as nc:   
        # extracting datatime metadata:
        time_unit = nc.variables['time'].units
        
        simulation_start = netCDF4.num2date(nc.variables['time'][0], time_unit)
        simulation_stop = netCDF4.num2date(nc.variables['time'][-1], time_unit)
    
    
        #h: layer thikness
        h = numpy.array(nc.variables['h'][..., 0, 0])
        h_accumulated = h.cumsum(axis=1)
        # getting the mid-point of each layer:
        depth_repr = h_accumulated - h_accumulated[:, -1, numpy.newaxis] - h/2
        #zs_model = h_cumsum - h/2
        
        #extracting information about z: depth:
        depth = depth_repr
        #depth = numpy.array(nc.variables['z'][..., 0, 0])
        
        depth_min= numpy.min(depth)
        depth_max= numpy.max(depth)
        
        # checking water level variation based on the depth of the deepest layer
        if numpy.std(depth[:,1]) < 0.02:
            varying_water_level = "False"
        else:
            varying_water_level = "True"
    
        dict_APIget["meta"] ={"simulation_start"        : simulation_start,
                              "simulation_stop"         : simulation_stop,
                              "varying_water_level"        : varying_water_level,
                              "depth_min"                  : depth_min,
                              "depth_max"                  : depth_max
                             }
    
        # getting overview of paramters:
        for var in nc.variables:
            ith_var = nc.variables[var]
            dict_APIget["variables"][var] = {"unit"      : ith_var.units,
                                             "longname"  : ith_var.long_name,
                                             "shape"     : ith_var.shape
                                            }
            
        #export get to textfile:
        # to a collected json:
           
    if config_file_get["output"]["API_get"] == "TRUE" and skip_output == None:
          
      output_lst = []  
      for variable, meta in dict_APIget["variables"].items():
          output_lst.append([variable, meta["longname"], meta["unit"]])
                              
      with open(os.path.join(config_file_get["paths"]["rootout"],"API_get.csv"),'w',newline='\n', encoding='utf-8') as out:
          wr = csv.writer(out, dialect='excel',)
          wr.writerow(["variable","descrip","unit"])
          wr.writerows(output_lst)

    elif config_file_get["output"]["API_get"] == "FALSE":
        pass

    else:
        if skip_output == True:
            print("output is skipped")
            
        else:
            print("wrong argument specified: %s" %config_file_get["output"]["API_get"])
            print("no main output is generated")
        pass 
        

        
        
    return dict_APIget   


def API_get_data(json_config_file):

    with open(json_config_file, 'r') as f:
        config_dict = json.load(f)
    
    config_file_get_data = config_dict
    """
    config_file_get_data
    """
    # getting the temporal period to extract:
    if config_file_get_data["period"]["start"] == "begin":
        
        simulation_start = API_get_basics(json_config_file, skip_output = True)["meta"]["simulation_start"]
        if isinstance(simulation_start,datetime.datetime):
            date_pased_from = simulation_start
        else:
            try:
                date_pased_from = datetime.strptime(simulation_start, '%Y-%m-%d %H:%M:%S')
            except:
                print("wrong format of datetime")
    else:     
        date_pased_from =  datetime.strptime(config_file_get_data["period"]["start"], '%Y-%m-%d %H:%M:%S') 
    
        #if date_pased_from < API_get_basics(json_config_file, skip_output = True)["meta"]["simulation_start"]:
        #    date_pased_from = API_get_basics(json_config_file, skip_output = True)["meta"]["simulation_start"]
            
        #else:
        #    pass
            
    
    
    if config_file_get_data["period"]["stop"] == "end":
        
        simulation_stop = API_get_basics(json_config_file, skip_output = True)["meta"]["simulation_stop"]
        if isinstance(simulation_stop,datetime.datetime):
            date_pased_to = simulation_stop
        else:
            try:
                date_pased_to = datetime.strptime(simulation_stop, '%Y-%m-%d %H:%M:%S')
            except:
                print("wrong format of datetime")
    else:     
        date_pased_to =  datetime.strptime(config_file_get_data["period"]["stop"], '%Y-%m-%d %H:%M:%S')
        
        #if date_pased_to < API_get_basics(json_config_file, skip_output = True)["meta"]["simulation_stop"]:
        #    date_pased_to = API_get_basics(json_config_file, skip_output = True)["meta"]["simulation_stop"]
            
        #else:
        #    pass
               
        
    dict_APIgetdata = {}
    dict_APIgetdata["data"] = {}
    with netCDF4.Dataset(config_file_get_data["paths"]["nc_file"]) as nc:
        ###############var_data_p = numpy.array(nc.variables[get_var][..., 0, 0])
        #******************************************************************************************************#    
        # getting times and apply boolean
        #******************************************************************************************************#
        nctime = nc.variables['time']
        t_centers = nctime[:]
        time_unit = nctime.units
        datetime_fmt_complete = netCDF4.num2date(nc.variables['time'][:], time_unit)
            
        valid_dt = numpy.array([date_pased_from <= t <= date_pased_to for t in datetime_fmt_complete], dtype=bool)            
        datetime_fmt_crop = [t for t, v in zip(datetime_fmt_complete, valid_dt) if v]
        
        if len(datetime_fmt_crop) == 0:
            print("no simulation data can be found for the given time period")
            pass
        
        else:
            # adding it to the extraction dict:
            dict_APIgetdata["datetimes"] = [t.strftime('%Y-%m-%d %H:%M:%S') for t in datetime_fmt_crop]
            
            # while the netCDF is open we extract the depth but only for the selected period:
            #h: layer thikness
            h = numpy.array(nc.variables['h'][valid_dt,:, 0, 0])
            h_accumulated = h.cumsum(axis=1)
            # getting the mid-point of each layer:
            depth_crop = h_accumulated - h_accumulated[:, -1, numpy.newaxis] - h/2
            #zs_model = h_cumsum - h/2
            #depth = numpy.array(nc.variables['z'][..., 0, 0])
            #depth_crop = numpy.array(nc.variables['z'][valid_dt,:, 0, 0])
        
            # then we create the boolean for depth:
            # for one depth (here we find the model layer depth closest to the specified depth:
            if config_file_get_data["depth"]["from"] == config_file_get_data["depth"]["to"]:     
                valid_z = numpy.array([d_ith == find_nearest(d_ith, config_file_get_data["depth"]["to"])  for d_ith in depth_crop], dtype=bool)
            else:
            # for depth range:           
                valid_z = numpy.array([(d_ith <= config_file_get_data["depth"]["from"]) & (d_ith >= config_file_get_data["depth"]["to"]) for d_ith in depth_crop], dtype=bool)    
        
            # in case the layer depths vary over time the number of depths by timestep may vary, hence a loop to get each
            # indidual timestep depths
            #depth_lst = []
            #for i, val in enumerate(valid_z):
            #    depth_lst.append(depth[i,val].tolist())
            
            # adding it to the extraction dict:
            dict_APIgetdata["depths"] = [depth_crop[i,val].tolist() for i, val in enumerate(valid_z)]    
            
            # we combine the valid arguments to create ONE single mask to extract data from
            #valid_dt_z = numpy.array([numpy.logical_and(valid_dt, i) for i in valid_z.T]).T
            
        
            #******************************************************************************************************#    
            # getting data for the selected variables with applied booleans on time and depth
            #******************************************************************************************************#
            print("getting data...")
            #kk = numpy.argwhere(numpy.array(valid_dt_z))
              
            
            for get_var in config_file_get_data["variables"]:
                print("getting variable: %s" %get_var)
                
                if len(nc.variables[get_var].shape) == 4: # meaning variying with depth
                    # we minimize the extraction of data with the datetime mask:
                    var_data = numpy.array(nc.variables[get_var][valid_dt,:, 0, 0])    
                    
                    # in case the layer depths vary over time the number of datapopint by timestep may vary, hence a loop 
                    # to get each indidual timestep datapoints
                    var_data_lst = []
                    for i, val in enumerate(valid_z):
                        var_data_lst.append(var_data[i,val].tolist())
                    
                
                elif len(nc.variables[get_var].shape) == 3: # meaning NO variying with depth
                    # we minimize the extraction of data with the datetime mask:
                    var_data = numpy.array(nc.variables[get_var][valid_dt, 0, 0])
                    
                    var_data_lst = var_data.tolist()
                
                # adding it to the extraction dict:    
                #dict_data = {get_var: var_data_lst }
                
                dict_APIgetdata["data"][get_var] = var_data_lst    
    

    #******************************************************************************************************#    
    # Exporting Data
    #******************************************************************************************************#
    
    # to a collected json:
    if config_file_get_data["output"]["collected_json"] == "TRUE":
            
        with open(os.path.join(config_file_get_data["paths"]["rootout"],"collected_output.json"), 'w') as outfile:
            json.dump(dict_APIgetdata, outfile, indent = 4, ensure_ascii = False)

    elif config_file_get_data["output"]["collected_json"] == "FALSE":
        pass

    else: 
        print("wrong argument specified: %s" %config_file_get_data["output"]["collected_json"])
        print("no main output is generated")
        pass

    # to individual files:
    if config_file_get_data["output"]["profiles"] == "TRUE":
        
      for variable, data in dict_APIgetdata["data"].items():
          output_lst = []
          for i, d_1 in enumerate(data):
              
              if isinstance(d_1, list):
                  for j, d_2 in enumerate(d_1):
                      output_lst.append([dict_APIgetdata["datetimes"][i], dict_APIgetdata["depths"][i][j], d_2])
              else:
                  output_lst.append([dict_APIgetdata["datetimes"][i], d_1])
              
                
          with open(os.path.join(config_file_get_data["paths"]["rootout"],str(variable+".txt")),'w',newline='\n', encoding='utf-8') as out:
              wr = csv.writer(out, delimiter='\t')
              wr.writerows(output_lst)
    
    elif config_file_get_data["output"]["profiles"] == "FALSE":
        pass

    else: 
        print("wrong argument specified: %s" %config_file_get_data["output"]["profiles"])
        print("no output is generated")
        pass
       
    
    return dict_APIgetdata



      
        

def find_nearest(array, value):
    array = numpy.asarray(array)
    idx = (numpy.abs(array - value)).argmin()
    return array[idx]







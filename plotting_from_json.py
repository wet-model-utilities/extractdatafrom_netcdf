# -*- coding: utf-8 -*-
"""
@author: au302246
"""

import plotly.graph_objs as go
import plotly
import json
import numpy

#######################################################################################################################
########################################################PLOTTING line##################################################
#######################################################################################################################

def plotting(config_json):

    with open(config_json, 'r') as f:
        config_dict = json.load(f)
    
    data_json       = config_dict["plotting"]["input_json"] 
    plot_out        = config_dict["plotting"]["plot_out"]
    depth_to_plot   = config_dict["plotting"]["depth_to_plot"]
    var_to_plot     = config_dict["plotting"]["var_to_plot"]

    with open(data_json, 'r') as f:
        data_dict = json.load(f)

    lst_plot = []
    for depth, value in zip(data_dict["depths"],data_dict["data"][var_to_plot]):
        ids =find_nearest_idx(depth, depth_to_plot)  
        lst_plot.append(value[ids])
        
              
    trace = []
       
    trace.append(go.Scatter(
        x=data_dict["datetimes"],
        xaxis = 'x1',
        y=lst_plot,
        #fill='tonexty',
        #mode= 'line',
        name = var_to_plot,
        showlegend = True,
        #visible = "legendonly",
        #hoverinfo ="skip",
        line = dict(
            color = ('rgb(47,79,79)'),
            width = 1,)
        ))
            

           
    fig = go.Figure(data=trace)#, layout = layout)
    #fig.update_layout(plot_bgcolor='white')
    plotly.offline.plot(fig, filename=plot_out)




def find_nearest_idx(array, value):
    array = numpy.asarray(array)
    idx = (numpy.abs(array - value)).argmin()
    return idx

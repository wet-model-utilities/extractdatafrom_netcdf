# ExtractDataFrom_NetCDF

Script functions to extract data from WET model runs based on netCDF output files.


## how to

The configuration file should be configured with:
    paths
    variables to extract
    depths to extract
    period of interest 
    how output should be stored
    
You may run the "API_get_basics" and inspect the "meta" returned to get info on
simuation period and depths

The variable names are exported to a csv file and can be copied into the config
file to get correct naming.

The script functions rely on the netCDF4 module an the plotly module. Make sure
they are installed before executing the main script "ExtractNetCDFmain.py" 

In the config file the section "output" has the following meaning:

"API_get"            : if set to TRUE a csv with avaliable variables are exported
"profiles"           : if set to TRUE output is saved as txt files
"collected_json"     : if set to TRUE a json file with output is saved

To plot specify the variable name and depth in the config file.